#pragma once
#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class LightControls: public florp::game::IBehaviour {
public:
	LightControls(const glm::vec3& speed) : IBehaviour(), mySpeed(speed) {};
	virtual ~LightControls() = default;

	virtual void Update(entt::entity entity) override;
	void SetControl(bool& cont);

private:
	glm::vec3 mySpeed;

	bool* allowControl;
};
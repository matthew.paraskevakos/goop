#include "LightControls.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/wrap.hpp>

template <typename T>
T wrap(const T& value, const T& min, const T& max) {
	//(((x - x_min) % (x_max - x_min)) + (x_max - x_min)) % (x_max - x_min) + x_min;
	T range = max - min;
	return glm::mod(glm::mod(value - min, range) + range, range) + min;
}

void LightControls::Update(entt::entity entity) {

	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();

	glm::vec3 translate = glm::vec3(0.0f);
	glm::vec2 rotation = glm::vec2(0.0f);
	if (*allowControl) {
		if (window->IsKeyDown(Key::W))
			translate.z -= 1.0f;
		if (window->IsKeyDown(Key::S))
			translate.z += 1.0f;
		if (window->IsKeyDown(Key::A))
			translate.x -= 1.0f;
		if (window->IsKeyDown(Key::D))
			translate.x += 1.0f;
		if (window->IsKeyDown(Key::LeftControl))
			translate.y -= 1.0f;
		if (window->IsKeyDown(Key::Space))
			translate.y += 1.0f;

	}
	

	translate *= Timing::DeltaTime * mySpeed;

	if (glm::length(translate) > 0) {
		translate = glm::mat3(transform.GetLocalTransform()) * translate;
		translate += transform.GetLocalPosition();
		transform.SetPosition(translate);
	}
	
}

void LightControls::SetControl(bool& cont) {
	allowControl = &cont;
}
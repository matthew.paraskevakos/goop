#include "Scene.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>

#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include <florp\game\Transform.h>
//#include "RotateBehaviour.h"
#include "CameraComponent.h"
#include "florp/app/Application.h"
//#include <ControlBehaviour.h>
#include <ShadowLight.h>
#include "PointLight.h"
#include "CameraControls.h"
//#include "LightFlickerBehaviour.h"
#include <imgui.h>
#include"LightControls.h"
florp::graphics::Texture2D::Sptr CreateSolidTexture(glm::vec4 color)
{
	using namespace florp::graphics;
	static std::unordered_map<glm::vec4, Texture2D::Sptr> cache;

	// If a texture for that color exists in the cache, return it
	if (cache.find(color) != cache.end())
		return cache[color];
	// Otherwise, we'll create a new texture, cache it, then return it
	else {
		// We'll disable essentially anything fancy for our single-pixel color
		Texture2dDescription desc = Texture2dDescription();
		desc.Width = desc.Height = 1;
		desc.Format = InternalFormat::RGBA8;
		desc.MagFilter = MagFilter::Nearest;
		desc.MinFilter = MinFilter::Nearest;
		desc.MipmapLevels = 1;
		desc.WrapS = desc.WrapT = WrapMode::ClampToEdge;

		// By using the float pixel type, we can simply feed in the address of our color
		Texture2dData data = Texture2dData();
		data.Width = data.Height = 1;
		data.Format = PixelFormat::Rgba;
		data.Type = PixelType::Float;
		data.Data = &color.r;

		// Create the texture, and load the single pixel data
		Texture2D::Sptr result = std::make_shared<Texture2D>(desc);
		result->SetData(data);

		// Store in the cache
		cache[color] = result;

		return result;
	}
}
void Scene::Initialize()
{

	florp::app::Application* app = florp::app::Application::Get();

	using namespace florp::game;
	using namespace florp::graphics;

	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	// We'll load in a monkey head to render something interesting
	MeshData data = ObjLoader::LoadObj("tank.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/forward.fs.glsl");
	shader->Link();

	Shader::Sptr emissiveShader = std::make_shared<Shader>();
	emissiveShader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	emissiveShader->LoadPart(ShaderStageType::FragmentShader, "shaders/forward-emissive.fs.glsl");
	emissiveShader->Link();



	// Load and set up our simple test material
	Material::Sptr tankMaterial = std::make_shared<Material>(shader);
	tankMaterial->Set("s_Albedo", Texture2D::LoadFromFile("tank.png", false, true, true));

	Material::Sptr marbleMat = std::make_shared<Material>(shader);
	marbleMat->Set("s_Albedo", Texture2D::LoadFromFile("marble.png", false, true, true));

	Material::Sptr sphereMat = std::make_shared<Material>(shader);
	sphereMat->Set("s_Albedo", CreateSolidTexture(glm::vec4(1.0f)));


	const int numTanks = 6;
	const float step = glm::two_pi<float>() / numTanks; // Determine the angle between monkeys in radians

	for (int ix = 0; ix < numTanks; ix++) {
		entt::entity test = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = tankMaterial;
		Transform& t = scene->Registry().get<Transform>(test);
		t.SetPosition(glm::vec3(glm::cos(step * ix) * 5.0f, 0.0f, glm::sin(step * ix) * 5.0f));
		t.SetScale(glm::vec3(0.3));
	}


	RenderBufferDesc mainColor = RenderBufferDesc();
	mainColor.ShaderReadable = true;
	mainColor.Attachment = RenderTargetAttachment::Color0;
	mainColor.Format = RenderTargetType::ColorRgb8;


	RenderBufferDesc normalBuffer = RenderBufferDesc();
	normalBuffer.ShaderReadable = true;
	normalBuffer.Attachment = RenderTargetAttachment::Color1;
	normalBuffer.Format = RenderTargetType::ColorRgb10; // Note: this format is 10 bits per component

	RenderBufferDesc emissiveBuffer = RenderBufferDesc();
	emissiveBuffer.ShaderReadable = true;
	emissiveBuffer.Attachment = RenderTargetAttachment::Color2;
	emissiveBuffer.Format = RenderTargetType::ColorRgb10; // Note: this format is 10 bits per component

	// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
	RenderBufferDesc depth = RenderBufferDesc();
	depth.ShaderReadable = true;
	depth.Attachment = RenderTargetAttachment::Depth;
	depth.Format = RenderTargetType::Depth32;

	// Our main frame buffer needs a color output, and a depth output
	FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight(), 4);// the g buffer
	buffer->AddAttachment(mainColor);	
	buffer->AddAttachment(normalBuffer);
	buffer->AddAttachment(emissiveBuffer);
	buffer->AddAttachment(depth);
	buffer->Validate();
	buffer->SetDebugName("MainBuffer");


	entt::entity lightEnt = scene->CreateEntity();
	PointLight& light = scene->Registry().assign<PointLight>(lightEnt);
	light.Color = glm::vec3(0.0f,0.0f,1.0f);
	light.Attenuation =  1.0f/10.0f;
	Transform& t = scene->Registry().get<Transform>(lightEnt);
	t.SetPosition(glm::vec3(0.0f,10.0f,0.0f));
	scene->AddBehaviour<LightControls>(lightEnt, glm::vec3(10.0f));
	scene->GetBehaviour<LightControls>(lightEnt)->SetControl(controlLights);


	// We'll create an entity, and attach a camera component to it
	entt::entity camera = scene->CreateEntity();
	CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
	cam.BackBuffer = buffer;
	cam.FrontBuffer = buffer->Clone();
	cam.IsMainCamera = true;
	cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 1000.0f);

	// We'll add our control behaviour so that we can fly the camera around
	scene->AddBehaviour<CameraControls>(camera, glm::vec3(1.0f));
	scene->GetBehaviour<CameraControls>(camera)->SetControl(controlLights);
	auto& camTransform = scene->Registry().get<Transform>(camera);
	camTransform.SetPosition(glm::vec3(15.0f, 15.0f, 15.0f));
	camTransform.LookAt(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	
		MeshData dataSphere = MeshBuilder::Begin();
		MeshBuilder::AddUvSphere(dataSphere, glm::vec3(0, 0, 0),10,5);
		Mesh::Sptr meshSphere = MeshBuilder::Bake(dataSphere);
		entt::entity lightFollower = scene->CreateEntity();
		RenderableComponent& renderableSpeher = scene->Registry().assign<RenderableComponent>(lightFollower);
		renderableSpeher.Mesh = meshSphere;
		renderableSpeher.Material = sphereMat;
		Transform& lt = scene->Registry().get<Transform>(lightFollower);
		
		lt.SetParent(lightEnt);

	{
		// Building the mesh
		MeshData data = MeshBuilder::Begin();
		MeshBuilder::AddAlignedCube(data, glm::vec3(0.0f, -1.0f, 0.0), glm::vec3(100.0f, 0.1f, 100.0f));
		Mesh::Sptr mesh = MeshBuilder::Bake(data);

		// Creating the entity and attaching the renderable 
		entt::entity entity = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = marbleMat;
	}

//	scene->AddBehaviour<CameraControls>(camera, glm::vec3(45.0f, 45.0f, 45.0f));

}
void Scene::RenderGUI()
{
	ImGui::Begin("Scene");
	ImGui::Checkbox("Control Light Pos: ", &controlLights);
	
	ImGui::End();

}
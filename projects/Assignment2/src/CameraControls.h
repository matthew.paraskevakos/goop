#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class CameraControls : public florp::game::IBehaviour {
public:
	CameraControls(const glm::vec3& speed) : IBehaviour(), mySpeed(speed), myYawPitch(glm::vec2(0.0f)) {};
	virtual ~CameraControls() = default;

	virtual void Update(entt::entity entity) override;
	void SetControl(bool& cont);

private:
	glm::vec3 mySpeed;
	glm::vec2 myYawPitch;
	bool* allowControl;
};
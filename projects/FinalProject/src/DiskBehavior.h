#pragma once
#include "DiskComponent.h"

#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>
#include <florp\game\Transform.h>
using namespace florp::game;

class DiskBehaviour : public florp::game::IBehaviour {
public:
	DiskBehaviour(const glm::vec3& speed) : IBehaviour(), mySpeed(speed) {


	};
	virtual ~DiskBehaviour() = default;

	virtual void Update(entt::entity entity) override;
	void FireDisk(entt::entity entity, glm::vec3 forward, glm::vec3 up, glm::vec3 pos);

private:
	glm::vec3 mySpeed;

};

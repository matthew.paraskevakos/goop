#include "ControlBehaviour.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "CameraComponent.h"
#include "TankTopComponent.h"
#include "DiskBehavior.h"
#include "DiskComponent.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/wrap.hpp>

template <typename T>
T wrap(const T& value, const T& min, const T& max) {
	//(((x - x_min) % (x_max - x_min)) + (x_max - x_min)) % (x_max - x_min) + x_min;
	T range = max - min;
	return glm::mod(glm::mod(value - min, range) + range, range )+ min;
}

void ControlBehaviour::Update(entt::entity entity) {
	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();

	glm::vec3 translate = glm::vec3(0.0f);
	glm::vec2 rotation = glm::vec2(0.0f);
	if (window->IsKeyDown(Key::W))
		translate.z -= 1.0f;
	if (window->IsKeyDown(Key::S))
		translate.z += 1.0f;
	/*if (window->IsKeyDown(Key::A))
		translate.x -= 1.0f;
	if (window->IsKeyDown(Key::D))
		translate.x += 1.0f;*/
	/*if (window->IsKeyDown(Key::LeftControl))
		translate.y -= 1.0f;
	if (window->IsKeyDown(Key::Space))
		translate.y += 1.0f;*/

	if (window->IsKeyDown(Key::A))
		rotation.x += 1.0f;
	if (window->IsKeyDown(Key::D))
		rotation.x -= 1.0f;
	/*if (window->IsKeyDown(Key::Up))
		rotation.y += 1.0f;
	if (window->IsKeyDown(Key::Down))
		rotation.y -= 1.0f;*/

	translate *= Timing::DeltaTime * mySpeed;
	rotation *= Timing::DeltaTime * 25.0f;

	if (glm::length(translate) > 0) {
		translate = glm::mat3(transform.GetLocalTransform()) * translate;
		translate += transform.GetLocalPosition();
		transform.SetPosition(translate);
	}
	if (glm::length(rotation) > 0) {
		rotation.x *= glm::abs(myYawPitch.y) > 90 ? -1 : 1;
		myYawPitch += rotation;
		myYawPitch = wrap(myYawPitch, glm::vec2(-180.0f), glm::vec2(180.0f));
		glm::quat rot = glm::angleAxis(glm::radians(myYawPitch.x), glm::vec3(0, 1, 0));
		rot = rot * glm::angleAxis(glm::radians(myYawPitch.y), glm::vec3(1, 0, 0));
		transform.SetRotation(rot);
	

	
	}

	auto view0 = CurrentRegistry().view<TankTopComponent>();
	for (const auto& entity0 : view0) {
		auto& TankTransf = CurrentRegistry().get<florp::game::Transform>(entity0);

		TankTransf.SetRotation(transform.GetRotation());
		TankTransf.SetPosition(transform.GetLocalPosition() + (transform.GetUp()*2.2f));
	
		auto view1 = CurrentRegistry().view<CameraComponent>();
	for (const auto& entity1 : view1) {
		 auto& camTransf = CurrentRegistry().get<florp::game::Transform>(entity1);
		 
		 camTransf.LookAt(TankTransf.GetLocalPosition()+(TankTransf.GetForward()*1000.0f),TankTransf.GetUp());
		 camTransf.SetPosition(TankTransf.GetLocalPosition()-(TankTransf.GetForward()*15.0f) + glm::vec3(0.0f, 4.5f, 0.0f));
		 
	 }
	auto view2 = CurrentRegistry().view<DiskComponent>();
	for (const auto& entity2 : view2) {
		if (window->IsKeyDown(Key::Space)) {
			auto& diskComp = CurrentRegistry().get<DiskComponent>(entity2);
			diskComp.fire = true;
			diskComp.pos = TankTransf.GetLocalPosition();
			diskComp.forward = TankTransf.GetForward();
			diskComp.up = TankTransf.GetUp();
		}

		

	}

	}
	

}

#include "DiskBehavior.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"

void DiskBehaviour::Update(entt::entity entity) {
	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	auto& diskComp = CurrentRegistry().get<DiskComponent>(entity);
	if (diskComp.fire) {


		diskComp.fire = false;
		transform.SetPosition(diskComp.pos);
		transform.LookAt(transform.GetLocalPosition() + (diskComp.forward * mySpeed), diskComp.up);

	}


	transform.SetPosition( transform.GetLocalPosition()+transform.GetForward()*Timing::DeltaTime * mySpeed);
}

void DiskBehaviour::FireDisk(entt::entity entity, glm::vec3 forward, glm::vec3 up, glm::vec3 pos) {
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	transform.SetPosition(pos);
	transform.LookAt(transform.GetLocalPosition()+(forward*mySpeed),up);


}
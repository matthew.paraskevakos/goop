#pragma once
#pragma once
#include <optional>
#include <memory>
#include <GLM/glm.hpp>
#include "FrameBuffer.h"

struct DiskComponent {
	
	glm::vec3  forward = glm::vec3(0, 0, 1);
	glm::vec3  up = glm::vec3(0, 1, 0);
	glm::vec3  pos = glm::vec3(0, 0, 0);

	bool fire = false;
	
};

#pragma once
#include <optional>
#include <memory>
#include <GLM/glm.hpp>
#include "FrameBuffer.h"

//This component is just used as a tag
struct TankTopComponent {
    glm::vec3 offset = glm::vec3(0.0f, 3.2f, 0.0f);
};
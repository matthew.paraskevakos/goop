#version 440

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec2 inScreenCoords;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D a_Sampler;

layout(binding = 1) uniform sampler3D lutTexture;
uniform float lutSize;

void main(){
vec3 rawCol = texture(a_Sampler,inUV).rgb;
//getting our screen color
vec3 scale = vec3((lutSize - 1.0)/lutSize);
//the scale of our lut based on the size
vec3 offset = vec3(1.0/(2.0*lutSize));
//the offset of each color based on lut size
vec3 lutColor = texture(lutTexture,scale*rawCol+offset).rgb;
//our looked up color
outColor = vec4(lutColor,1.0);
}
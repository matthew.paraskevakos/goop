#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class BallBehaviour : public florp::game::IBehaviour {
public:
	BallBehaviour();
	~BallBehaviour() = default;

	virtual void Update(entt::entity entity) override;
};

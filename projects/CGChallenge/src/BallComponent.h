#pragma once
#include <GLM/glm.hpp>

struct BallComponent {
public:
	float radius;
	glm::vec3 velocity;
	float speed;
};
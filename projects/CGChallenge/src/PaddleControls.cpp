#include "PaddleControls.h"
#include "PaddleComponent.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"


void PaddleControls::Update(entt::entity entity)
{
	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();

	glm::vec3 translate = glm::vec3(0.0f);
	if (playerID) {
		if (window->IsKeyDown(Key::W))
			translate.y += 1.0f;
		if (window->IsKeyDown(Key::S))
			translate.y -= 1.0f;
	}
	else {
		if (window->IsKeyDown(Key::Up))
			translate.y += 1.0f;
		if (window->IsKeyDown(Key::Down))
			translate.y -= 1.0f;
	}
	translate *= Timing::DeltaTime* CurrentRegistry().get<PaddleComponent>(entity).speed;
	if (glm::length(translate) > 0) {
		translate = glm::mat3(transform.GetLocalTransform()) * translate;
		translate += transform.GetLocalPosition();
		transform.SetPosition(translate);
	}
}
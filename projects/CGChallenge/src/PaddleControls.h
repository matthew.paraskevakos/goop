#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class PaddleControls : public florp::game::IBehaviour {
public:
	PaddleControls(bool id) : playerID(id) {};
	~PaddleControls() = default;

	virtual void Update(entt::entity entity) override;
private:
	bool playerID;

};

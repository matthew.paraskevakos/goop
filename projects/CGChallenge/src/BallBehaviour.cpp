#include "BallComponent.h"
#include "BallBehaviour.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"

BallBehaviour::BallBehaviour()
{
}

void BallBehaviour::Update(entt::entity entity)
{
	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	BallComponent& ball = CurrentRegistry().get<BallComponent>(entity);
	Window::Sptr window = Application::Get()->GetWindow();

	glm::vec3 translate = ball.velocity;

	translate *= Timing::DeltaTime*ball.speed;
	translate = glm::mat3(transform.GetLocalTransform()) * translate;
	translate += transform.GetLocalPosition();
	transform.SetPosition(translate);
}
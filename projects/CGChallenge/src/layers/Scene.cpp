#include "Scene.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>

#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include <florp\game\Transform.h>
//#include "RotateBehaviour.h"
#include "CameraComponent.h"
#include "PaddleComponent.h"
#include "BallComponent.h"
#include "florp/app/Application.h"
//#include <ControlBehaviour.h>
#include <ShadowLight.h>
#include "PointLight.h"
#include "BallBehaviour.h"
#include "PaddleControls.h"
//#include "CameraControls.h"
//#include "LightFlickerBehaviour.h"
#include <imgui.h>

//#include"LightControls.h"
ShadowLight& CreateShadowCaster(florp::game::Scene* scene, entt::entity* entityOut, glm::vec3 pos, glm::vec3 target, glm::vec3 up, float distance = 10.0f, float fov = 60.0f, glm::ivec2 bufferSize = { 1024, 1024 }, const char* name = nullptr)
{
	// The depth attachment is a texture, with 32 bits for depth
	RenderBufferDesc depth = RenderBufferDesc();
	depth.ShaderReadable = true;
	depth.Attachment = RenderTargetAttachment::Depth;
	depth.Format = RenderTargetType::Depth32;

	// Our shadow buffer is depth-only
	FrameBuffer::Sptr shadowBuffer = std::make_shared<FrameBuffer>(bufferSize.x, bufferSize.y);
	shadowBuffer->AddAttachment(depth);
	shadowBuffer->Validate();
	if (name != nullptr)
		shadowBuffer->SetDebugName(name);

	// Create a new entity
	entt::entity entity = scene->CreateEntity();

	// Assign and initialize a shadow light component
	ShadowLight& light = scene->Registry().assign<ShadowLight>(entity);
	light.ShadowBuffer = shadowBuffer;
	light.Projection = glm::perspective(glm::radians(fov), (float)bufferSize.x / (float)bufferSize.y, 0.25f, distance);
	light.Attenuation = 1.0f / distance;
	light.Color = glm::vec3(1.0f);

	// Assign and initialize the transformation
	florp::game::Transform& t = scene->Registry().get<florp::game::Transform>(entity);
	t.SetPosition(pos);
	t.LookAt(target, up);

	// Send out the entity ID if we passed in a place to store it
	if (entityOut != nullptr)
		*entityOut = entity;

	return light;
}


florp::graphics::Texture2D::Sptr CreateSolidTexture(glm::vec4 color)
{
	using namespace florp::graphics;
	static std::unordered_map<glm::vec4, Texture2D::Sptr> cache;

	// If a texture for that color exists in the cache, return it
	if (cache.find(color) != cache.end())
		return cache[color];
	// Otherwise, we'll create a new texture, cache it, then return it
	else {
		// We'll disable essentially anything fancy for our single-pixel color
		Texture2dDescription desc = Texture2dDescription();
		desc.Width = desc.Height = 1;
		desc.Format = InternalFormat::RGBA8;
		desc.MagFilter = MagFilter::Nearest;
		desc.MinFilter = MinFilter::Nearest;
		desc.MipmapLevels = 1;
		desc.WrapS = desc.WrapT = WrapMode::ClampToEdge;

		// By using the float pixel type, we can simply feed in the address of our color
		Texture2dData data = Texture2dData();
		data.Width = data.Height = 1;
		data.Format = PixelFormat::Rgba;
		data.Type = PixelType::Float;
		data.Data = &color.r;

		// Create the texture, and load the single pixel data
		Texture2D::Sptr result = std::make_shared<Texture2D>(desc);
		result->SetData(data);

		// Store in the cache
		cache[color] = result;

		return result;
	}
}
void Scene::Initialize()
{

	florp::app::Application* app = florp::app::Application::Get();

	using namespace florp::game;
	using namespace florp::graphics;

	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	MeshData paddleMesh = ObjLoader::LoadObj("paddle.obj", glm::vec4(1.0f));
	MeshData ballMesh = ObjLoader::LoadObj("ball.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/forward.fs.glsl");
	shader->Link();

	Shader::Sptr emissiveShader = std::make_shared<Shader>();
	emissiveShader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	emissiveShader->LoadPart(ShaderStageType::FragmentShader, "shaders/forward-emissive.fs.glsl");
	emissiveShader->Link();



	

	Material::Sptr marbleMat = std::make_shared<Material>(shader);
	marbleMat->Set("s_Albedo", Texture2D::LoadFromFile("marble.png", false, true, true));

/*	Material::Sptr blankMat = std::make_shared<Material>(shader);
	blankMat->Set("s_Albedo", Texture2D::LoadFromFile("blankTexture.png", false, true, true));*/


	

	RenderBufferDesc mainColor = RenderBufferDesc();
	mainColor.ShaderReadable = true;
	mainColor.Attachment = RenderTargetAttachment::Color0;
	mainColor.Format = RenderTargetType::ColorRgb8;


	RenderBufferDesc normalBuffer = RenderBufferDesc();
	normalBuffer.ShaderReadable = true;
	normalBuffer.Attachment = RenderTargetAttachment::Color1;
	normalBuffer.Format = RenderTargetType::ColorRgb10; // Note: this format is 10 bits per component

	RenderBufferDesc emissiveBuffer = RenderBufferDesc();
	emissiveBuffer.ShaderReadable = true;
	emissiveBuffer.Attachment = RenderTargetAttachment::Color2;
	emissiveBuffer.Format = RenderTargetType::ColorRgb10; // Note: this format is 10 bits per component

	// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
	RenderBufferDesc depth = RenderBufferDesc();
	depth.ShaderReadable = true;
	depth.Attachment = RenderTargetAttachment::Depth;
	depth.Format = RenderTargetType::Depth32;

	// Our main frame buffer needs a color output, and a depth output
	FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight(), 4);// the g buffer
	buffer->AddAttachment(mainColor);	
	buffer->AddAttachment(normalBuffer);
	buffer->AddAttachment(emissiveBuffer);
	buffer->AddAttachment(depth);
	buffer->Validate();
	buffer->SetDebugName("MainBuffer");


/*	entt::entity lightEnt = scene->CreateEntity();
	PointLight& light = scene->Registry().assign<PointLight>(lightEnt);
	light.Color = glm::vec3(0.0f,0.0f,1.0f);
	light.Attenuation =  1.0f/10.0f;
	Transform& t = scene->Registry().get<Transform>(lightEnt);
	t.SetPosition(glm::vec3(0.0f,10.0f,0.0f));*/
	//scene->AddBehaviour<LightControls>(lightEnt, glm::vec3(10.0f));
	//scene->GetBehaviour<LightControls>(lightEnt)->SetControl(controlLights);
	{
		MeshData data = MeshBuilder::Begin();
		MeshBuilder::AddAlignedCube(data, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.5f, 2.0f, 1.0f));
		
		entt::entity playerOne = scene->CreateEntity();
		RenderableComponent& renderable1 = scene->Registry().assign<RenderableComponent>(playerOne);
		renderable1.Mesh =  MeshBuilder::Bake(data);
		renderable1.Material = marbleMat;
		Transform& t1 = scene->Registry().get<Transform>(playerOne);
		t1.SetPosition(glm::vec3(-10.0f, 0.0f, 20.0f));
		//t1.SetEulerAngles(glm::vec3(0.0, 0.0, 90.0f));
		PaddleComponent& paddleComp1 = scene->Registry().assign<PaddleComponent>(playerOne);
		paddleComp1.lives = 3;
		paddleComp1.halfHeight = 2;
		paddleComp1.halfWidth = 0.5;
		paddleComp1.speed = 10.0f;
		scene->AddBehaviour<PaddleControls>(playerOne, false);

	}
	{
		MeshData data = MeshBuilder::Begin();
		MeshBuilder::AddAlignedCube(data, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.5f, 2.0f, 1.0f));

		entt::entity playerOne = scene->CreateEntity();
		RenderableComponent& renderable1 = scene->Registry().assign<RenderableComponent>(playerOne);
		renderable1.Mesh = MeshBuilder::Bake(data);
		renderable1.Material = marbleMat;
		Transform& t1 = scene->Registry().get<Transform>(playerOne);
		t1.SetPosition(glm::vec3(10.0f, 0.0f, 20.0f));
		//t1.SetEulerAngles(glm::vec3(0.0, 0.0, 90.0f));
		PaddleComponent& paddleComp1 = scene->Registry().assign<PaddleComponent>(playerOne);
		paddleComp1.lives = 3;
		paddleComp1.halfHeight = 2;
		paddleComp1.halfWidth = 0.5;
		paddleComp1.speed = 10.0f;
		scene->AddBehaviour<PaddleControls>(playerOne, true);

	}

	{
		

		entt::entity playerOne = scene->CreateEntity();
		RenderableComponent& renderable1 = scene->Registry().assign<RenderableComponent>(playerOne);
		renderable1.Mesh = MeshBuilder::Bake(ballMesh);
		renderable1.Material = marbleMat;
		Transform& t1 = scene->Registry().get<Transform>(playerOne);
		t1.SetPosition(glm::vec3(0.0f, 0.0f, 20.0f));
		//t1.SetEulerAngles(glm::vec3(0.0, 0.0, 90.0f));
		BallComponent& ballComp = scene->Registry().assign<BallComponent>(playerOne);
		ballComp.radius = 3.0;
		ballComp.velocity = glm::vec3(-1.0f, 0.01, 0.0f);
		ballComp.speed = 5.0f;
		scene->AddBehaviour<BallBehaviour>(playerOne);

	}
	entt::entity lightEnt = entt::null;
	auto& light = CreateShadowCaster(scene, &lightEnt, glm::vec3(0.0f, 5.0f, 7.0f), glm::vec3(0.0f, 0.0f, 35.0f), glm::vec3(0.0f, 1.0f, 0.0f), 45.0f, 75.0f);
	// We'll generate a color for the light
	light.Color = glm::vec3(1.0f, 0.64f, 0.0f) * 0.2f;
	light.Attenuation = 1.0f / 40.0f;

	// We'll create an entity, and attach a camera component to it
	entt::entity camera = scene->CreateEntity();
	CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
	cam.BackBuffer = buffer;
	cam.FrontBuffer = buffer->Clone();
	cam.IsMainCamera = true;
	cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 1000.0f);

	// We'll add our control behaviour so that we can fly the camera around
	//scene->AddBehaviour<CameraControls>(camera, glm::vec3(1.0f));
	//scene->GetBehaviour<CameraControls>(camera)->SetControl(controlLights);
	auto& camTransform = scene->Registry().get<Transform>(camera);
	camTransform.SetPosition(glm::vec3(0,0,0));
	camTransform.LookAt(glm::vec3(0, 0, 15), glm::vec3(0, 1, 0));

	{
		// Building the mesh
		MeshData data = MeshBuilder::Begin();
		MeshBuilder::AddAlignedCube(data, glm::vec3(0.0f, 0.0f,25.0f), glm::vec3(13.0f,13.0f, 1.0f));
		Mesh::Sptr mesh = MeshBuilder::Bake(data);

		// Creating the entity and attaching the renderable 
		entt::entity entity = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = marbleMat;
	}
	
	{
		// Building the mesh
		MeshData data = MeshBuilder::Begin();
		MeshBuilder::AddAlignedCube(data, glm::vec3(0.0f, 0.0f, 20.0f), glm::vec3(1.0f, 1.0f, 1.0f));
		Mesh::Sptr mesh = MeshBuilder::Bake(data);

		// Creating the entity and attaching the renderable 
		entt::entity entity = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = marbleMat;
	}


}
void Scene::RenderGUI()
{


}

void Scene::Update() {
	using namespace florp::game;
	using namespace florp::graphics;
	florp::app::Application* app = florp::app::Application::Get();
	auto& ecs = CurrentRegistry();
	auto ballView = ecs.view<BallComponent>();
	auto paddleView = ecs.view<PaddleComponent>();
	for (const auto& entity : ballView) {
		Transform& ball = ecs.get<Transform>(entity);
		glm::vec3 ballPos = ball.GetLocalPosition();
		BallComponent& ballComp = ecs.get<BallComponent>(entity);
		for (const auto& entity : paddleView) {
			Transform& paddle = ecs.get<Transform>(entity);
			glm::vec3 paddlePos = paddle.GetLocalPosition();
			PaddleComponent& paddleComp = ecs.get<PaddleComponent>(entity);

			if (paddlePos.y + paddleComp.halfHeight >= 8)
				paddlePos.y = app->GetWindow()->GetHeight() - paddleComp.halfHeight;
			else if (paddlePos.y - paddleComp.halfHeight <= 0)
				paddlePos.y = paddleComp.halfHeight;

			if(paddlePos.x + paddleComp.halfWidth >= ballPos.x - ballComp.radius &&
				paddlePos.x - paddleComp.halfWidth <= ballPos.x + ballComp.radius&&
				paddlePos.y + paddleComp.halfHeight >= ballPos.y - ballComp.radius &&
				paddlePos.y - paddleComp.halfHeight <= ballPos.y + ballComp.radius){
				ballComp.velocity.x *= -1.0f;
				float fifth = (paddleComp.halfHeight * 2.0f) / 5.0f;
				if (ballPos.y >= paddlePos.y - (fifth * 0.5f) && ballPos.y <= paddlePos.y + (fifth * 0.5f))
					ballComp.velocity.y = (ballComp.velocity.y < 0) ? 0.5f : -0.5f;
				else if (ballPos.y >= paddlePos.y + (fifth * 0.5f) && ballPos.y <= paddlePos.y + (fifth * 1.5f))
					ballComp.velocity.y = 1.0f;
				else if (ballPos.y <= paddlePos.y - (fifth * 0.5f) && ballPos.y >= paddlePos.y - (fifth * 1.5f))
					ballComp.velocity.y = -1.0f;
				else if (ballPos.y >= paddlePos.y + (fifth * 1.5f) && ballPos.y <= paddlePos.y + paddleComp.halfHeight)
					ballComp.velocity.y = 2.0f;
				else if (ballPos.y <= paddlePos.y - (fifth * 1.5f) && ballPos.y >= paddlePos.y - paddleComp.halfHeight)
					ballComp.velocity.y = 2.0f;
			}
			else if (paddlePos.x < app->GetWindow()->GetWidth()/2 && ballPos.x + ballComp.radius <=0) {
				ball.SetPosition(glm::vec3(glm::vec3(0.0f, 0.0f, 20.0f)));
				ballComp.velocity = (glm::vec3(1.0f, -0.5f, 0.0f));
				paddleComp.lives -= 1;
			}
			else if (paddlePos.x > app->GetWindow()->GetWidth()/2 && ballPos.x - ballComp.radius >= app->GetWindow()->GetWidth()) {
				ball.SetPosition(glm::vec3(0.0f,0.0f,20.0f));
				ballComp.velocity = (glm::vec3(-1.0f, -0.5f, 0.0f));
				paddleComp.lives -= 1;
			}
		}
		if (ballPos.y <= 0 || ballPos.y >= app->GetWindow()->GetHeight())
			ballComp.velocity.y *= 1.0f;

	}


}
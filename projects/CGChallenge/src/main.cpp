#include "florp/app/Application.h"
#include "florp/game/BehaviourLayer.h"
#include "florp/game/ImGuiLayer.h"
#include "layers/Scene.h"
#include "layers/Rendering.h"
#include "layers/PostProcessing.h"
#include "layers//AudioLayer.h"
#include "layers/Lighting.h"
#include "florp/graphics/TextureCube.h"



int main()
{
	{
		// Create our application
		florp::app::Application* app = new florp::app::Application();

		// Set up our layers
		app->AddLayer<florp::game::BehaviourLayer>();
		app->AddLayer<florp::game::ImGuiLayer>();
	//	app->AddLayer<AudioLayer>();
		app->AddLayer<Scene>();
		app->AddLayer<Renderer>();
		app->AddLayer<Lighting>();
		app->AddLayer<PostProcessing>();

		app->Run();

		delete app;
	}

	return 0;
}
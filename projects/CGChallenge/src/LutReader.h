#pragma once
#include <istream>
#include<fstream>
#include <string>
#include <vector>
#include <glad/glad.h>
#include <florp\graphics\Shader.h>
#include <florp\graphics\ITexture.h>
using namespace std;

struct RGB
{
	float r,g,b;


};
class LUT {
public:
	LUT() {}
	~LUT() {
	}

	void OpenLut(const string path) {
		vector<RGB> LUT;

		ifstream LUTfile(path.c_str());
		while (!LUTfile.eof()) {
			string LUTline;
			getline(LUTfile, LUTline);
			if (LUTline.empty()) continue;
			RGB line;
			if (sscanf(LUTline.c_str(), "%f %f %f", &line.r, &line.g, &line.b) == 3) LUT.push_back(line);
		}
		if (LUT.size() != (pow(LUTsize, 3.0))) {

		}
		// Create a 3D texture
			// Reference from http://content.gpwiki.org/index.php/OpenGL:Tutorials:3D_Textures
		glEnable(GL_TEXTURE_3D);

		glGenTextures(1, &texture3D);
		glBindTexture(GL_TEXTURE_3D, texture3D);

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);

		glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, LUTsize, LUTsize, LUTsize, 0, GL_RGB,
			GL_FLOAT, &LUT[0]);

		glBindTexture(GL_TEXTURE_3D, 0);
		glDisable(GL_TEXTURE_3D);

	}

	void ReadLut(int loc, florp::graphics::Shader::Sptr myShader) {
		glActiveTexture(GL_TEXTURE0 + loc);

		glEnable(GL_TEXTURE_3D);
		glBindTexture(GL_TEXTURE_3D, texture3D);
		glDisable(GL_TEXTURE_3D);

		myShader->SetUniform("lutTexture", loc);
		glActiveTexture(GL_TEXTURE0);
	}
	
private:
	int LUTsize = 32;
	GLuint texture3D;
};

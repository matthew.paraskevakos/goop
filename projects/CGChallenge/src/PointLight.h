#pragma once#pragma once
#include <GLM/glm.hpp>

struct PointLight {
	glm::vec3 Color;
	float     Attenuation;
};

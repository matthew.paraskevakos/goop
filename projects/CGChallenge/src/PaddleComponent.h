#pragma once

struct PaddleComponent{
public:
	float halfWidth;
	float halfHeight;
	int lives;
	float speed;
};